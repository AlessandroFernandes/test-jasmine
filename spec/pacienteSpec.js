describe("Paciente", () => {

    let alessandro;
    beforeEach(() => {
       alessandro = new PacienteBuild().getPeso(88).build();
    });

    it("Calculo de IMC", () => {
        expect(alessandro.imc()).toEqual('27.16');
    });

    it("Calculo de Imc por formula", () => {
        expect(alessandro.imc()).toEqual((88 / (1.80 * 1.80)).toFixed(2))
    });

    it("Teste de batimento", ()=> {
        expect(alessandro.batimento()).toEqual(1244160000)
    })

})