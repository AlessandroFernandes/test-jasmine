function PacienteBuild(){

    let nome = "Alessandro";
    let idade = 30;
    let peso = 88;
    let altura = 1.80;

    let clazz = {

        getNome: function(value){
            nome = value;
            return this;
        },
        getIdade : function(value){
            idade = value;
            return this;
        },
        getPeso : function(value){
            peso = value;
            return this;
        },
        getAltura : function(value){
            altura = value;
            return this;
        },
        build : () => new paciente(nome, idade, peso, altura)
    };
    return clazz;
}