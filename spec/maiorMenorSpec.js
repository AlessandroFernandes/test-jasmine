describe("maiorMenor", () => {

    it('Teste de lista Crescente', () => {
        let test = new maiorMenor();
        test.media([1,10,20,40,50]);
        expect(test.getMenor()).toEqual(1);
        expect(test.getMaior()).toEqual(50);
    });
    it('Teste de lista Decrescente', () => {
        let test = new maiorMenor();
        test.media([50,40,20,30,10]);
        expect(test.getMenor()).toEqual(10);
        expect(test.getMaior()).toEqual(50);
    });
    it('Teste de lista Desordenada', () => {
        let test = new maiorMenor();
        test.media([3,9,2,60,1]);
        expect(test.getMenor()).toEqual(1);
        expect(test.getMaior()).toEqual(60);
    });
    it("Teste um item", () => {
        let test = new maiorMenor();
        test.media([1]);
        expect(test.getMenor()).toEqual(1);
        expect(test.getMaior()).toEqual(1);
    });
    it('Teste de ordenação', () => {
        let test = new maiorMenor();
        expect(test.getOrdena([30,2,10])).toEqual([2,10,30]);
    });

})