describe("Consulta", () => {

    let alessandro = new PacienteBuild().build();

    it("Consultar - Retorno", () => {
        const test = new consulta(alessandro, "consultaSimples", false, true);
        expect(test.preco()).toEqual(0);
    });

    it("Consultar valor de exames simples", () => {
        const test = new consulta(alessandro, ['soro'], false, false);
        expect(test.preco()).toEqual(25)
    });

    it("Consulta particular consulta simples", () =>{
        const test = new consulta(alessandro, ['soro', 'injecao'], true, false);
        expect(test.preco()).toEqual(100);
    });

    it("Cosulta particular exame especial", ()=>{
        const test = new consulta(alessandro, ['gesso'], true, false);
        expect(test.preco()).toEqual(200);
    })

    it("Teste de exame com equavalência", () => {
        const test = new consulta(alessandro, ['gesso'], true, false);
        expect(test.preco()).toEqual(100 * 2)
    })
})