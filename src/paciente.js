function paciente(nome, idade, peso, altura){

    const clazz = {
        infomacao : () => alert(`Nome ${nome} idade ${idade}`),
        batimento : () => idade * 360 * 24 * 60 * 80,
        imc : () => (peso / (altura * altura)).toFixed(2)
    };
    return clazz;
}