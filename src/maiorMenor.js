function maiorMenor(){

    var menor;
    var maior;

    var teste = {
        media : function(nums){

            menor = Number.MAX_VALUE;
            maior = Number.MIN_VALUE;

            nums.forEach(function(num){
                if(num < menor) menor = num;
                if(num > maior) maior = num;
            });
        },
        getMenor : function(){return menor},
        getMaior : function(){return maior},
        getOrdena : function(nums){
            nums.sort((a,b) => a - b);
            return nums;
        }
    };
    return teste;
};