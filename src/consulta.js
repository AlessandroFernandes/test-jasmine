function consulta(paciente, procedimentos, particular, retorno){

    let test = {
        preco : () => {
            if(retorno) { return 0 };

            let precoTotal = 0;

            procedimentos.forEach(proc => {
                if('raio-x' == proc) precoTotal += 50;
                else if('gesso' == proc) precoTotal += 100;
                else precoTotal += 25;
            });

            if(particular) { precoTotal *= 2};

            return precoTotal;
        }
    }
    return test;
}