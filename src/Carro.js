class  Carro{

    constructor(nome, velocidade){
        this.nome = nome;
        this.velocidade = velocidade;
    };

    mostrar(){
        return this.nome;
    };

    velocidade(){
        return alert(`Velocidade do carro ${this.velocidade}`)
    }

    ultrapassou(){
       return  alert("Velocidade em doblo " + this.velocidade * 2)
    }

}